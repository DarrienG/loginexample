#!/usr/bin/env bash

docker build -t loginexample .
docker run --rm -e "PORT"="$PORT" -p"$PORT":"$PORT" loginexample
