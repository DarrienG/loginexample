CREATE DATABASE IF NOT EXISTS logindb;
CREATE USER 'loginexample'@'localhost';
GRANT ALL PRIVILEGES ON logindb.* TO 'loginexample'@'localhost';

CREATE TABLE IF NOT EXISTS logindb.users (
    email VARCHAR(150) NOT NULL UNIQUE,
    hashword CHAR(128) NOT NULL,
    PRIMARY KEY(email, hashword)
);

CREATE TABLE IF NOT EXISTS logindb.sessions (
    email VARCHAR(320) NOT NULL,
    token CHAR(36) NOT NULL UNIQUE,
    PRIMARY KEY(token)
);
