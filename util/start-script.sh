#!/usr/bin/env bash

set -e

echo "Initializing MySQL. This will take about 30 seconds..."

mysqld &

while ! mysqladmin ping -h"localhost"; do
    sleep 1
done

echo "MySQL initialized!"

echo "Setting up tables and users"
mysql -u root < /util/tables.sql

echo "Starting server..."
python3 /server.py
