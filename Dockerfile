FROM python:3.8.0-buster

RUN apt-get update -y && \
        apt-get install -y \
        default-mysql-server \
        dumb-init

COPY /requirements.txt /requirements.txt

RUN pip3 install -r /requirements.txt

COPY /util /util
RUN mkdir -p /var/run/mysqld && \
        chmod 777 -R /var/run

COPY /server.py /server.py
COPY /src /src

ENTRYPOINT ["dumb-init", "--"]
CMD ["bash", "/util/start-script.sh"]
