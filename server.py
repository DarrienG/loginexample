#!/usr/bin/env python3
"""Entrypoint to server"""

import os
from flask import Flask, request, jsonify
import MySQLdb
import src.login_controller as lic
import src.register_controller as regc
import src.logout_controller as logc
import src.auth_errors.auth_errors as ae

APP = Flask(__name__)
DB = MySQLdb.connect(db="logindb", user="loginexample", host="0.0.0.0")


@APP.route("/")
def default():
    """Default page to route user to other endpoints"""
    return "Nothing exciting here. Check out /login, /register, or /logout"


@APP.route("/login", methods=["GET", "POST"])
def login():
    """/login endpoint"""
    user_req = request.json
    email = user_req["email"]
    password = user_req["password"]

    try:
        return jsonify({"token": lic.process(DB, email, password)}), 200
    except ae.UserDoesNotExistError:
        return jsonify({"token": "USER DOES NOT EXIST"}), 404
    except ae.IncorrectPassword:
        return jsonify({"token": "INVALID USERNAME OR PW"}), 403

    return jsonify({"token": "ERROR"}), 500


@APP.route("/register", methods=["GET", "POST"])
def register():
    """/register endpoint"""
    user_req = request.json
    email = user_req["email"]
    password = user_req["password"]

    try:
        regc.process(DB, email, password)
        return jsonify({"msg": "SUCCESSFULLY REGISTERED"}), 200
    except ae.UserAlreadyExists:
        return jsonify({"msg": "USER ALREADY EXISTS"}), 400
    except ae.MalformedEmail:
        return jsonify({"msg": "MALFORMED EMAIL"}), 400

    return jsonify({"msg": "ERROR"}), 500


@APP.route("/logout", methods=["GET", "POST"])
def logout():
    """/logout endpoint"""
    user_req = request.json
    token = user_req["token"]
    try:
        logc.process(DB, token)
        return jsonify({"msg": "SUCCESSFULLY LOGGED OUT"}), 200
    except ae.TokenDoesNotExist:
        return jsonify({"msg": "USER TRIED TO LOG OUT WITH TOKEN DOES NOT EXIST"}), 400


def main():
    """Entrypoint to server"""
    DB.autocommit(True)
    port = os.environ["PORT"]
    if not port:
        port = 5000
    APP.run(host="0.0.0.0", port=port)


if __name__ == "__main__":
    main()
