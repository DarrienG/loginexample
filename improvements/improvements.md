# Improvements

There are quite a few.

## Configurability

For one, none of the parameters in the application are configurable. This makes
it inconvenient to run on a server with many users if not using something like
docker.

## Connection handling

When started, the server [only gets one
connection](https://gitlab.com/DarrienG/yakstack-python/blob/master/server.py#L12)
and uses it for the lifetime of the app.

This isn't a huge issue performance wise for a flask app, because flask is more
or less single threaded, but if running with uwsgi/nginx or apache to manage
processes, this becomes a big performance issue.

What is more concerning is that if the single connection is ever dropped (e.g.
connection reset), everything from there on out fails.

For some of my [longer term
projects](https://gitlab.com/yak-stack/yaklet-webserver/blob/master/src/main/kotlin/yaklet/webserver/dbConn/ConnectionPool.kt)
I've written my own connectionPool implementations. This would drastically
improve database handling.

## Speed

The whole app is running using the Flask dev server. Since this is more or less
a quick demo, it doesn't matter too much, but in production this would be a big
issue.

## Tests

There are no tests. I started writing some integration tests with
[testcontainers-python](https://github.com/testcontainers/testcontainers-python)
but the library doesn't play nice sometimes, and isn't nearly as fast as the
Java version, so I scrapped it. For some of my [other bigger
projects](https://gitlab.com/yak-stack/yaklet-webserver/blob/master/src/test/kotlin/yaklet/webserver/endpointController/AbstractDatabaseTest.kt),

## Docker usage

In the quick spinup container if you're keen, you might notice there's a MySQL
container and a Flask server running in there.

Running more than one process in a docker container is generally considered a
bad practice. In "real life" you would generally have an external MySQL server
somewhere and never do something like this, but for the sake of a tech demo, it
makes it easy to show off.

## JSON safety

If you don't pass in the parameters the webserver are looking for, it will yell
at you. Parameters should really be handled more gracefully, but for the sake of
a tech demo, this part is ommitted.
