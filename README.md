# Sample login application

A quick example of a containerized server written in Python.

Want to jump right in before reading? If you have docker installed, you can do:

```bash
bash ./startup-no-deps.sh
```

And everything will begin running on port 5000.

Note that data will NOT be persisted in this mode.

It will take probably a minute or so to build and start everything.

## Usage

The simple login server does some very basic registration and login, and also
lets users log out.

The webserver implements 3 endpoints. '/register', '/login', and 'logout'

Register and login both accept `POST` requests with a JSON body:

```json
{
    "email": "$EMAIL",
    "password": "$PASSWORD"
}
```

Register sends back a small `msg`:

```
{
    "msg": "Successfully registered!"
}
```

And login sends back a login `token` if you successfully log in:

```json
{
  "token": "bc98ab8214004613b3fa10be79f94459"
}
```

If you hit an error condition, the msg is more or less replaced with a message.
Proper status codes are used.

/logout takes the same token used and removes the user's session from the
database.

```json
{
	"token" : "bc98ab8214004613b3fa10be79f94459"
}
```

It too returns a `msg`:

```json
{
  "msg": "SUCCESSFULLY LOGGED OUT"
}
```

And happily tells you you logged out whether or not you gave it a valid token.

## Running

Run with no deps:

```bash
bash ./startup-no-deps.sh
```

Or if you want to run locally, Python 3.8 and MySQL or MariaDB are required. The
MySQL user must be able to create users, tables, databases, and grant
permissions.

To run:

```bash
pip3 install -r requirements.txt
mysql -u root < utils/tables.sql
python3 ./server.py
```

And the webserver will begin running on port 5000.

## Technologies used

* Python 3.8 with extensive type hinting
* Flask: webserver
* mysqlclient: database connections
* bcrypt: Securely hashing passwords

Note that the app is not vulnerable to SQL injections. Because it uses bcrypt
the database is not particularly vulnerable to things like rainbow tables.

## Contributing

Enjoyed the project? We'd love more contributors!

All code MUST pass pylint, and auto formatting with the
[Black](https://github.com/psf/black) formatter.

This is enforced in the CI job.

## What could be improved?

Well a lot. Check out the improvements folder for a short writeup on that.
