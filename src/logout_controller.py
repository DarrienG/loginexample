#!/usr/bin/env python3
"""
Implementation of logout
Pluggable and does not need to be used with flask
"""

import MySQLdb
import src.auth_helper as ah
import src.auth_errors.auth_errors as ae


def process(conn: MySQLdb, token: str):
    """Logout entrypoint"""
    if not ah.check_token_exists(conn, token):
        raise ae.TokenDoesNotExist
    remove_user_token(conn, token)



def remove_user_token(conn: MySQLdb, token: str):
    """Remove session token"""
    cursor = conn.cursor()
    cursor.execute("DELETE FROM sessions WHERE token = %s", (token,))
    cursor.close()
