#!/usr/bin/env python3
"""
Implementation of login
Pluggable and does not need to be used with flask
"""

import uuid
import bcrypt
import MySQLdb
import src.auth_helper as auth_helper
import src.auth_errors.auth_errors as ae


def process(conn: MySQLdb, email: str, password: str) -> str:
    """Login entrypoint"""
    if not auth_helper.check_user_exists(conn, email):
        raise ae.UserDoesNotExistError
    if not password_correct(conn, email, password):
        raise ae.IncorrectPassword

    return store_token(conn, email, generate_token())


def password_correct(conn: MySQLdb, email: str, password: str) -> bool:
    """Check to see if user password is correct"""
    cursor = conn.cursor()
    cursor.execute("SELECT hashword FROM users WHERE email = %s", (email,))

    hashword = cursor.fetchone()[0]
    cursor.close()

    return bcrypt.checkpw(password.encode("UTF-8"), hashword.encode("UTF-8"))


def generate_token() -> str:
    """Generate unique session token"""
    return uuid.uuid4().hex


def store_token(conn: MySQLdb, email: str, token: str) -> str:
    """Store user token in sessions table"""
    cursor = conn.cursor()
    cursor.execute("INSERT INTO sessions(email, token) VALUES(%s, %s)", (email, token))
    cursor.close()
    return token
