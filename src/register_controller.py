#!/usr/bin/env python3
"""
Implementation of register
Pluggable and does not need to be used with flask
"""

from email.utils import parseaddr
import bcrypt
import MySQLdb
import src.auth_helper as auth_helper
import src.auth_errors.auth_errors as ae


def process(conn: MySQLdb, email: str, password: str) -> str:
    """Register entrypoint"""
    if auth_helper.check_user_exists(conn, email):
        raise ae.UserAlreadyExists
    if not valid_email(email):
        raise ae.MalformedEmail

    register_user(conn, email, hash_password(password))


def valid_email(email: str) -> bool:
    """Determine if email is valid"""
    return parseaddr(email)[1] != ""


def hash_password(password: str) -> str:
    """Hash password and return as string"""
    return bcrypt.hashpw(password.encode("UTF-8"), bcrypt.gensalt())


def register_user(conn: MySQLdb, email: str, hashword: str):
    """Register user in database"""
    cursor = conn.cursor()
    cursor.execute(
        "INSERT INTO users (email, hashword) VALUES(%s, %s)", (email, hashword)
    )
    cursor.close()
