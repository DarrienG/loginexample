#!/usr/bin/env python3
"""
Set of reuseable functions across all auth controllers
"""

import MySQLdb


def check_user_exists(conn: MySQLdb, email: str) -> bool:
    """Check to see if user exists in database"""
    cursor = conn.cursor()
    cursor.execute("SELECT COUNT(*) FROM users WHERE email = %s", (email,))

    exists = cursor.fetchone()[0] > 0
    cursor.close()

    return exists


def check_token_exists(conn: MySQLdb, token: str) -> bool:
    """Checks to see if the user exists"""
    cursor = conn.cursor()
    cursor.execute("SELECT COUNT(1) FROM logindb.sessions WHERE token = %s", (token,))

    exists = cursor.fetchone()[0] > 0
    cursor.close()

    return exists
