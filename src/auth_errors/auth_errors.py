#!/usr/bin/env python3

"""Exceptions used during authentication"""


class UserDoesNotExistError(Exception):
    """Exception for when user does not exist"""


class IncorrectPassword(Exception):
    """Exception for when user password is incorrect"""


class UserAlreadyExists(Exception):
    """Exception for when user already exists"""


class MalformedEmail(Exception):
    """Exception for when email address is malformed"""

class TokenDoesNotExist(Exception):
    """Exception for when token does not exist"""
